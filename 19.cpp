﻿#include <iostream>


class Animal 
{
   
    
public:
   Animal()
   {}

    virtual void Voice()
    {
        std::cout << "Voice\n";
    }

    virtual ~Animal()
    {
        std::cout << "delete memory Animal\n";
    }
   
};

class Dog : public Animal
{

public:
    Dog()
    {}

    void Voice() override
    {
        std::cout << "Woof\n";
    }

    ~Dog() override
    {
        std::cout << "delete memory Dog\n";
    }
};

class Cat : public Animal 
{

public:
    Cat ()
    {}

    void Voice() override
    {
        std::cout << "Meow\n";
    }

    ~Cat() override
    {
        std::cout << "delete memory Cat\n";
    }
};

class Goat : public Animal
{

public:
    Goat()
    {}

    void Voice() override
    {
        std::cout << "Be\n";
    }

    ~Goat() override
    {
        std::cout << "delete memory Goat\n";
    }
};


int main()
{
    
    Animal* Arr [3];

    Animal* p = new Dog;

    Arr[0] = p;

    Animal* p1 = new Cat;

    Arr[1] = p1;

    Animal* p2 = new Goat;

    Arr[2] = p2;
     
   
    for (int i = 0; i<3; i++)
    {
       Arr[i]->Voice();
              
     }

    delete p;
    delete p1;
    delete p2;

    
    
    
}